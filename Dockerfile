# base image
ARG ARCH=amd64
FROM $ARCH/debian:buster-slim

# args
ARG VCS_REF
ARG BUILD_DATE

# environment
ENV WIREGUARD_VERSION=v1.0.20200413

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/k8s-wireguard-module" \
  org.label-schema.description="Ensure wireguard module is installed on all Kubernetes nodes" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/k8s-wireguard-module" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/k8s-wireguard-module" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install dependencies
RUN \
  apt-get update -qq && \
  apt-get install -y -qq apt-utils libelf-dev build-essential pkg-config kmod bc git software-properties-common ubuntu-keyring && \
  apt-key add /usr/share/keyrings/ubuntu-archive-keyring.gpg && \
  rm -rf /var/lib/apt/lists/*

# clone wireguard
RUN git clone --depth=1 --branch="${WIREGUARD_VERSION}" https://git.zx2c4.com/wireguard-linux-compat /usr/local/src/wireguard

# add install script
COPY install.sh /opt/install.sh

# default command
CMD ["/opt/install.sh"]
