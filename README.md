# k8s-wireguard-module

Automatically install wireguard kernel module on all nodes.

Currently support nodes running Debian, Ubuntu and RancherOS.
If the kernel headers are available it should also work on other operating systems.

## Installation

```bash
kubectl apply -f https://gitlab.com/ydkn/k8s-wireguard-module/raw/master/daemonset.yaml
```

### RancherOS

Enabling the kernel-headers service is required:

```bash
sudo ros service enable kernel-headers
sudo ros service up kernel-headers
sudo ros config set rancher.modules "['wireguard']"
```
