#!/bin/bash -e

# autoload wireguard module on boot
function enable_autoload_module() {
  if [ -f /host/etc/modules ]; then
    if ! grep -q ^wireguard$ /host/etc/modules; then
      echo "wireguard" >>/host/etc/modules
      echo "Wireguard module added to /etc/modules"
    fi
  else
    echo "/etc/modules does not exist on host"
  fi
}

# check if wireguard module already loaded
if lsmod | grep ^wireguard &>/dev/null; then
  echo "Wireguard module already loaded"

  exit 0
fi

# check if wireguard module already is available
if modinfo wireguard &>/dev/null; then
  echo "Wireguard module already installed"

  # load wireguard module
  modprobe wireguard
  echo "Wireguard module loaded"

  # enable autoload
  enable_autoload_module

  exit 0
fi

# determine running operating system
for f in /host/etc/os-release /host/usr/lib/os-release /host/usr/share/ros/os-release; do
  if [ -L "${f}" ]; then
    if [ -e "${f}" ]; then
      target=$(readlink -f "${f}")

      if [[ "${target}" == "/host/*" ]]; then
        source "${f}"
        break
      fi
    fi
  else
    source "${f}"
    break
  fi
done

if [ -z "${ID}" ]; then
  echo "Type of running operarting system cloud not be determined"

  exit 1
fi

# install kernel headers for running kernel on debian systems
function install_kernel_headers_debian() {
  # check if kernel headers already installed
  if [ -d "/lib/modules/$(uname -r)/build" ]; then
    exit 0
  fi

  # check if header package exists
  if ! apt-cache pkgnames | grep -q "^linux-headers-$(uname -r)$"; then
    echo "Header package for current kernel could not be found: linux-headers-$(uname -r)"

    exit 1
  fi

  apt-get install -y -qq "linux-headers-$(uname -r)"

  echo "Kernel headers linux-headers-$(uname -r) installed"
}

# compile wireguard module from source
function compile() {
  if [ ! -d "/lib/modules/$(uname -r)" ]; then
    echo "Kernel not found: /lib/modules/$(uname -r)"

    exit 1
  fi

  if [ ! -d "/lib/modules/$(uname -r)/build" ]; then
    echo "Kernel build directory not found: /lib/modules/$(uname -r)/build"

    exit 1
  fi

  make -C /usr/local/src/wireguard/src -j$(nproc) module
  make -C /usr/local/src/wireguard/src -j$(nproc) module-install
}

case "${ID}" in
debian)
  # setup repositories
  echo "deb http://deb.debian.org/debian ${VERSION_CODENAME} main" >/etc/apt/sources.list.d/${VERSION_CODENAME}.list
  echo "deb http://security.debian.org/debian-security ${VERSION_CODENAME}/updates main" >>/etc/apt/sources.list.d/${VERSION_CODENAME}.list
  echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-updates main" >>/etc/apt/sources.list.d/${VERSION_CODENAME}.list
  echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-backports main" >/etc/apt/sources.list.d/backports.list
  echo "deb http://deb.debian.org/debian unstable main" >/etc/apt/sources.list.d/unstable.list

  # set apt preferences
  printf "Package: *\nPin: release a=${VERSION_CODENAME}-backports\nPin-Priority: 900\n" >/etc/apt/preferences.d/backports
  printf "Package: *\nPin: release a=unstable\nPin-Priority: 90\n" >/etc/apt/preferences.d/unstable

  # update package lists
  apt-get update -y -qq

  # install kernel headers
  install_kernel_headers_debian

  # install wireguard kernel module
  apt-get install -y -qq wireguard-dkms
  echo "Wireguard module installed"
  ;;
ubuntu)
  # setup repositories
  echo "deb http://archive.ubuntu.com/ubuntu ${VERSION_CODENAME} main" >/etc/apt/sources.list.d/${VERSION_CODENAME}.list
  echo "deb http://archive.ubuntu.com/ubuntu ${VERSION_CODENAME}-updates main" >>/etc/apt/sources.list.d/${VERSION_CODENAME}.list
  echo "deb http://archive.ubuntu.com/ubuntu ${VERSION_CODENAME}-backports main" >/etc/apt/sources.list.d/backports.list
  echo "deb http://security.ubuntu.com/ubuntu ${VERSION_CODENAME}-security main" >>/etc/apt/sources.list.d/${VERSION_CODENAME}.list

  # update package lists
  apt-get update -y -qq

  # install kernel headers
  install_kernel_headers_debian

  # check if wireguard-dkms package is available
  if apt-cache pkgnames | grep -q "^wireguard-dkms$"; then
    # install wireguard kernel module
    apt-get install -y -qq wireguard-dkms
    echo "Wireguard module installed"
  else
    # install wireguard PPA
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AE33835F504A1A25
    add-apt-repository -y ppa:wireguard/wireguard

    # update package lists
    apt-get update -y -qq

    # install wireguard kernel module
    apt-get install -y -qq wireguard-dkms
    echo "Wireguard module installed"
  fi
  ;;
rancheros)
  compile
  ;;
*)
  compile
  ;;
esac

# load wireguard module
modprobe wireguard
echo "Wireguard module loaded"

# enable autoload
enable_autoload_module

exit 0
